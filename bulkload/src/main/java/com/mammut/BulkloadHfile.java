package com.mammut;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Objects;

/**
 * @author semon
 * @version v1.0
 * @date 2019-09-04 14:54
 * @description note
 **/
public class BulkloadHfile {
    private static final  Logger logger = LoggerFactory.getLogger(BulkloadHfile.class);

    public static void main(String[] args) throws Exception {
        if(args.length != 2) {
            System.out.println("Usage: java -cp HfileGenerate-1.0-SNAPSHOT.jar com.mammut.BulkloadHfile tableName hdfsPath");
            System.exit(-1);
        }

        TableName tablename = TableName.valueOf(args[0]);
        Path path = new Path(args[1]);

        logger.info("NameSpace: {}",tablename.getNamespaceAsString());
        logger.info("Table Name: {}",tablename.getNameAsString());
        logger.info("Path {}",path.toUri().toString());

        Configuration conf = new Configuration(false);
        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        for (File file : Objects.requireNonNull(new File(System.getProperty("user.dir") + "/conf").listFiles())) {
            if(file.getAbsolutePath().endsWith("xml")) {
                logger.info("load config file {}" ,file.getAbsolutePath());
                conf.addResource(new Path(file.getAbsolutePath()));
            }
        }

        Connection conn = ConnectionFactory.createConnection(conf);

        LoadIncrementalHFiles loaderHfile = new LoadIncrementalHFiles(conf);

        loaderHfile.doBulkLoad(path,conn.getAdmin(),conn.getTable(tablename),conn.getRegionLocator(tablename));

        System.out.println("Hfile has bulkloaded successful!");
    }

}
