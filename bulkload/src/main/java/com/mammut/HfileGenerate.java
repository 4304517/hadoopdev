package com.mammut;

/**
 * @author semon
 * @version v1.0
 * @date 2019-09-06 17:04
 * @description note
 **/


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

public class HfileGenerate {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {


        if(args.length < 3) {
            System.err.println("Usage:com.mammut.HfileGenerate：<infilepath> <outfilepath> <tablename> [reduceTaskNum]");
            System.exit(-1);
        }

        Configuration conf = HBaseConfiguration.create();

        for (File f: Objects.requireNonNull(new File(System.getProperty("user.dir") + "/conf").listFiles()))
        {
            if (f.getAbsolutePath().endsWith(".xml"))
            {
                conf.addResource( new Path(f.getAbsolutePath()));
            }
        }



        //创建链接
        Connection connection = ConnectionFactory.createConnection(conf);

        //获取参数：第一个参数是输入文件的路劲，第二个参数是hfile文件存储路径参数，第三个是hbase表表名
        String[] dfsArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

        //判断Hfile生成路径是否存在，如存在则删除
        FileSystem fs = FileSystem.get(conf);
        Path hfileDir = new Path(dfsArgs[1]);
        if(fs.isDirectory(hfileDir)) {
           fs.delete(hfileDir,true);
           System.out.println("***********************************************************************************");
           System.out.println("The DestHdfsDir "+ hfileDir.toString() +" is already exists, Successfully deleted! ");
           System.out.println("**********************************************************************************");
        }

        //上传数据文件及hbase表参数至hdfs
        String hdfsPath = uploadLocalFile(conf);


        //设置job
        Job job = Job.getInstance(conf, "HFile Generator");

        if(args[4].length() != 0) {
            job.setNumReduceTasks(Integer.parseInt(args[4]));
        }

        //添加配置文件至分布式缓存中
        job.addCacheFile(new Path(hdfsPath).toUri());



        job.setJarByClass(HfileGenerate.class);

        //设置mapper信息
        job.setMapperClass(HfileMapper.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(Put.class);

        //设置文件输入输出路径
        FileInputFormat.addInputPath(job, new Path(dfsArgs[0]));
        FileOutputFormat.setOutputPath(job, hfileDir);

        //获取Hbase表region对应rowkey范围
        HTableDescriptor table = new HTableDescriptor(dfsArgs[2]);

        HFileOutputFormat2.configureIncrementalLoad(job, table, connection.getRegionLocator(TableName.valueOf(dfsArgs[2])));

        //启动job 并判断job执行结果
        boolean flag = job.waitForCompletion(true);
        if(!flag) {
            System.exit(-1);
        }

    }


    public static String uploadLocalFile(Configuration conf) throws IOException {

        String filename = "fileConf.conf";
        String localPath = System.getProperty("user.dir");
        String preFilename = localPath.substring(localPath.lastIndexOf("/")+1) + "_" + filename;
        boolean flag = new File(localPath + "/conf/" + filename).renameTo(new File(localPath + "/conf/" + preFilename));
        if (!flag) {
            System.exit(-1);
        }
        String dstDir = "/tmp/fileconf/";
        FileSystem fs = FileSystem.get(conf);
        if(!fs.isDirectory(new Path(dstDir))) {
            fs.mkdirs(new Path(dstDir));
        }

        fs.copyFromLocalFile(new Path(localPath+"/conf/"+preFilename),new Path(dstDir));
        String fsPath = dstDir +  preFilename;
        System.out.println("DistributedCachef file is :" + fsPath);
        return fsPath;
    }
}