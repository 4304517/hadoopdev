package com.mammut;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author semon
 * @version v1.0
 * @date 2019-09-09 10:12
 * @description note
 **/




public class HfileMapper extends Mapper<LongWritable,Text,ImmutableBytesWritable, Put> {

    Properties prop = new Properties();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        FileSystem fs = FileSystem.get(context.getConfiguration());

        URI[] fileConf = context.getCacheFiles();

        InputStream is = null;
        for(URI uri: fileConf) {
            if (uri.getPath().endsWith("conf")) {
                is = new BufferedInputStream(fs.open(new Path(uri.getPath())));
                prop.load(is);
            }
        }

    }

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String separate = prop.getProperty("separate");
        String[] cols = prop.getProperty("columns").split(",");
        byte[] rowkey = Bytes.toBytes(prop.getProperty("rowkey"));
        String familyName = prop.getProperty("familyName");
        String[] rows = value.toString().split(separate);


        Map<String,Integer> colMap = new HashMap();

        //将字段名转换为map
        for (int i=0;i<cols.length;i++){
            colMap.put(cols[i],i);
        }

        byte[] family = Bytes.toBytes(familyName);

        Put put = new Put(rowkey);

        //拼接hbase column
        for(String s :cols) {
            if(s == "rowkey") {
                continue;
            }
            put.addColumn(family,Bytes.toBytes(s),Bytes.toBytes(rows[colMap.get(s)]));
            System.out.println(rows[colMap.get(s)]);
        }

        context.write(new ImmutableBytesWritable(rowkey),put);
    }
}
